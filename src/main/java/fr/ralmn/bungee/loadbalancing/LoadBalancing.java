package fr.ralmn.bungee.loadbalancing;

import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: ralmn
 * Date: 29/09/13
 * Time: 19:03
 * To change this template use File | Settings | File Templates.
 */
public class LoadBalancing extends Plugin implements Listener {

    HashMap<String, Boolean> serversUp = new HashMap<String, Boolean>();

    @Override
    public void onDisable() {

    }

    @Override
    public void onEnable() {
        this.getProxy().getPluginManager().registerListener(this, this);
        for (final ServerInfo serverInfo : getProxy().getServers().values()) {
            declarePing(serverInfo);
        }
        this.getProxy().registerChannel("bungeetp");

    }

    private void declarePing(final ServerInfo serverInfo1) {
        this.getProxy().getScheduler().schedule(this, new Runnable() {
            @Override
            public void run() {
                serverInfo1.ping(new Callback<ServerPing>() {
                    @Override
                    public void done(ServerPing result, Throwable error) {
                        serversUp.put(serverInfo1.getName(), error == null);
                      //  getProxy().getConsole().sendMessage(ChatColor.YELLOW + serverInfo1.getName() + " : " + (error == null));
                    }
                });
            }

        }, 0, 10, TimeUnit.SECONDS);
    }

    public ServerInfo getBestTarget(final ProxiedPlayer player) {
       return getBestTarget(player, null);
    }


    public ServerInfo getBestTarget(final ProxiedPlayer player, ServerInfo si) {
        ServerInfo best = null;
        List<ServerInfo> servers = new ArrayList<ServerInfo>();
        for (ServerInfo serverInfo : this.getProxy().getServers().values()) {
            servers.add(serverInfo);
        }
        Collections.reverse(servers);
        for (final ServerInfo serverInfo : servers) {
            if (best == null && serverInfo.canAccess(player) && (serversUp.containsKey(serverInfo.getName()) && serversUp.get(serverInfo.getName()))) {
                best = serverInfo;
                continue;
            }
            if ((best != null && best.getPlayers().size() > serverInfo.getPlayers().size()) && serversUp.get(serverInfo.getName()) && serverInfo.canAccess(player) && (si == null ||si != serverInfo )) {
                best = serverInfo;
            }
        }
        return best;
    }

    @EventHandler
    public void onServerConnect(final ServerConnectEvent event) {
        final ServerInfo best = getBestTarget(event.getPlayer());
        if (best == null) {
            event.getPlayer().disconnect(ChatColor.RED + "Serveur plein ... veuillez ressayer");
            return;
        }

        getProxy().getConsole().sendMessage(/*ChatColor.YELLOW +*/ best.getName() + "(" + best.getAddress().toString() + ")");
        event.setTarget(best);

    }


    @EventHandler
    public void onServerTurnOff(final ServerKickEvent event){
        getProxy().getConsole().sendMessage(ChatColor.YELLOW + event.getPlayer().getName() + "(" + event.getState().toString() + " - " + event.getKickReason() + ")");
        if(event.getKickReason().contains("closed")){
            ServerInfo best = getBestTarget(event.getPlayer(), event.getCancelServer());
            event.getPlayer().setReconnectServer(best);
            event.setCancelled(true);
            event.setCancelServer(best);
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
            }
            event.getPlayer().sendMessage(ChatColor.YELLOW + "Le serveur sur lequel vous vous trouviez a été stoppé, nous vous avons donc tranferé sur ce serveur-ci.");
        }
    }


}
